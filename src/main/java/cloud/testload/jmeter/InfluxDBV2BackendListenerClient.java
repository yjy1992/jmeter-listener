package cloud.testload.jmeter;

import cloud.testload.jmeter.config.influxdb.InfluxDBConfig;
import cloud.testload.jmeter.config.influxdb.RequestMeasurement;
import cloud.testload.jmeter.config.influxdb.TestStartEndMeasurement;
import cloud.testload.jmeter.config.influxdb.VirtualUsersMeasurement;
import com.influxdb.client.BucketsApi;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.WriteApi;
import com.influxdb.client.domain.Bucket;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.visualizers.backend.AbstractBackendListenerClient;
import org.apache.jmeter.visualizers.backend.BackendListenerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Backend listener that writes JMeter metrics to influxDB 2.x directly.
 *
 * @author Alexander Wert
 * @author Alexander Babaev (minor changes and improvements)
 * @author jieying yang
 *
 */
public class InfluxDBV2BackendListenerClient extends AbstractBackendListenerClient implements Runnable {
	/**
	 * Logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ClickHouseBackendListenerClient.class);

	/**
	 * Buffer.
	 */
	private static final List<SampleResult> allSampleResults = new ArrayList<SampleResult>();

	/**
	 * Parameter Keys.
	 */
	private static final String KEY_USE_REGEX_FOR_SAMPLER_LIST = "useRegexForSamplerList";
	private static final String KEY_TEST_NAME = "testName";
	private static final String KEY_RUN_ID = "runId";
	private static final String KEY_NODE_NAME = "nodeName";
	private static final String KEY_SAMPLERS_LIST = "samplersList";
	private static final String KEY_RECORD_SUB_SAMPLES = "recordSubSamples";
	private static final String KEY_RECORD_GROUP_BY = "groupBy";
	private static final String KEY_RECORD_GROUP_BY_COUNT = "groupByCount";

	/**
	 * Constants.
	 */
	private static final String SEPARATOR = ";";
	private static final int ONE_MS_IN_NANOSECONDS = 1000000;

	/**
	 * Scheduler for periodic metric aggregation.
	 */
	private ScheduledExecutorService scheduler;

	/**
	 * Name of the test.
	 */
	private String testName;

	/**
	 * A unique identifier for a single execution (aka 'run') of a load test.
	 * In a CI/CD automated performance test, a Jenkins or Bamboo build id would be a good value for this.
	 */
	private String runId;

	/**
	 * Name of the name
	 */
	private String nodeName;

	/**
	 * List of samplers to record.
	 */
	private String samplersList = "";

	/**
	 * Regex if samplers are defined through regular expression.
	 */
	private String regexForSamplerList;

	/**
	 * Set of samplers to record.
	 */
	private Set<String> samplersToFilter;

	/**
	 * InfluxDB configuration.
	 */
	InfluxDBConfig influxDBConfig;

	/**
	 * influxDB client.
	 */
	private InfluxDBClient influxDB;

	/**
	 * influxDB write api.
	 */
	private WriteApi writeApi;

	/**
	 * influxDB buckets api.
	 */
	private BucketsApi bucketsApi;

	/**
	 * Random number generator
	 */
	private Random randomNumberGenerator;

	/**
	 * Indicates whether to record Subsamples
	 */
	private boolean recordSubSamples;

	/**
	 * Indicates whether to aggregate statistic
	 */
	private boolean groupBy;
	private int groupByCount;
	@Override
	public void run() {
		try {
			JMeterContextService.ThreadCounts tc = JMeterContextService.getThreadCounts();
			addVirtualUsersMetrics(getUserMetrics().getMinActiveThreads(), getUserMetrics().getMeanActiveThreads(), getUserMetrics().getMaxActiveThreads(), tc.startedThreads, tc.finishedThreads);
		} catch (Exception e) {
			LOGGER.error("Failed writing to influx", e);
		}
	}

	/**
	 * Write thread metrics.
	 */
	private void addVirtualUsersMetrics(int minActiveThreads, int meanActiveThreads, int maxActiveThreads, int startedThreads, int finishedThreads) {
		Point point = Point.measurement(VirtualUsersMeasurement.MEASUREMENT_NAME).time(System.currentTimeMillis(), WritePrecision.MS);
		point.addField(VirtualUsersMeasurement.Fields.MIN_ACTIVE_THREADS, minActiveThreads);
		point.addField(VirtualUsersMeasurement.Fields.MAX_ACTIVE_THREADS, maxActiveThreads);
		point.addField(VirtualUsersMeasurement.Fields.MEAN_ACTIVE_THREADS, meanActiveThreads);
		point.addField(VirtualUsersMeasurement.Fields.STARTED_THREADS, startedThreads);
		point.addField(VirtualUsersMeasurement.Fields.FINISHED_THREADS, finishedThreads);
		point.addTag(VirtualUsersMeasurement.Tags.NODE_NAME, nodeName);
		point.addTag(VirtualUsersMeasurement.Tags.TEST_NAME, testName);
		point.addTag(VirtualUsersMeasurement.Tags.RUN_ID, runId);
		getWriteApi().writePoint(influxDBConfig.getInfluxDatabase(), influxDBConfig.getInfluxOrgId(), point);
	}

	@Override
	public void setupTest(BackendListenerContext context) {
		testName = context.getParameter(KEY_TEST_NAME, "Test");
		runId = context.getParameter(KEY_RUN_ID,"R001"); //Will be used to compare performance of R001, R002, etc of 'Test'
		randomNumberGenerator = new Random();
		nodeName = context.getParameter(KEY_NODE_NAME, "Test-Node");

		groupBy = context.getBooleanParameter(KEY_RECORD_GROUP_BY, false);
		groupByCount = context.getIntParameter(KEY_RECORD_GROUP_BY_COUNT, 100);


		setupInfluxClient(context);
		getWriteApi().writePoint(
				influxDBConfig.getInfluxDatabase(),
				influxDBConfig.getInfluxOrgId(),
				Point.measurement(TestStartEndMeasurement.MEASUREMENT_NAME).time(System.currentTimeMillis(), WritePrecision.MS)
						.addTag(TestStartEndMeasurement.Tags.TYPE, TestStartEndMeasurement.Values.STARTED)
						.addTag(TestStartEndMeasurement.Tags.NODE_NAME, nodeName)
						.addTag(TestStartEndMeasurement.Tags.TEST_NAME, testName)
						.addField(TestStartEndMeasurement.Fields.PLACEHOLDER, "1"));

		parseSamplers(context);
		scheduler = Executors.newScheduledThreadPool(1);

		scheduler.scheduleAtFixedRate(this, 1, 1, TimeUnit.SECONDS);

		// Indicates whether to write sub sample records to the database
		recordSubSamples = Boolean.parseBoolean(context.getParameter(KEY_RECORD_SUB_SAMPLES, "false"));

	}

	@Override
	public Arguments getDefaultParameters() {
		Arguments arguments = new Arguments();
		arguments.addArgument(KEY_TEST_NAME, "Test");
		arguments.addArgument(KEY_NODE_NAME, "Test-Node");
		arguments.addArgument(KEY_RUN_ID, "R001");
		arguments.addArgument(InfluxDBConfig.KEY_INFLUX_DB_HOST, "localhost");
		arguments.addArgument(InfluxDBConfig.KEY_INFLUX_DB_PORT, Integer.toString(InfluxDBConfig.DEFAULT_PORT));
		arguments.addArgument(InfluxDBConfig.KEY_INFLUX_DB_USER, "admin");
		arguments.addArgument(InfluxDBConfig.KEY_INFLUX_DB_PASSWORD, "12345678");
		arguments.addArgument(InfluxDBConfig.KEY_INFLUX_DB_TOKEN, "swPagVU09621sWxsgR40AIFXAxPWwOrqW22Xdg1nPTiaXG6yhqAkfT82yCTFuuj-rEPboN0Gi3c0f8tTyErhEQ==");
		arguments.addArgument(InfluxDBConfig.KEY_INFLUX_DB_ORG_ID, "b3ca2f2fb2a85a26");
		arguments.addArgument(InfluxDBConfig.KEY_INFLUX_DB_DATABASE, "jmeter");
		arguments.addArgument(InfluxDBConfig.KEY_RETENTION_POLICY, InfluxDBConfig.DEFAULT_RETENTION_POLICY);
		arguments.addArgument(KEY_SAMPLERS_LIST, ".*");
		arguments.addArgument(KEY_USE_REGEX_FOR_SAMPLER_LIST, "true");
		arguments.addArgument(KEY_RECORD_SUB_SAMPLES, "true");
		arguments.addArgument(KEY_RECORD_GROUP_BY, "false");
		arguments.addArgument(KEY_RECORD_GROUP_BY_COUNT, "100");
		return arguments;
	}

	/**
	 * Setup influxDB client.
	 *
	 * @param context
	 *            {@link BackendListenerContext}.
	 */
	private void setupInfluxClient(BackendListenerContext context) {
		influxDBConfig = new InfluxDBConfig(context, InfluxDBConfig.VERSION_V2);
		influxDB = InfluxDBClientFactory.create(
				influxDBConfig.getInfluxDBURL(InfluxDBConfig.VERSION_V2),
				influxDBConfig.getInfluxToken().toCharArray(),
				influxDBConfig.getInfluxDatabase(),
				influxDBConfig.getInfluxDatabase());
		writeApi = influxDB.makeWriteApi();
		bucketsApi = influxDB.getBucketsApi();
		createDatabaseIfNotExistent();
	}

	/**
	 * 数据库 api
	 * @return
	 */
	public BucketsApi getBucketsApi() {
		if (bucketsApi == null) {
			bucketsApi = influxDB.getBucketsApi();
		}
		return bucketsApi;
	}

	/**
	 * 数据写入 api
	 * @return
	 */
	public WriteApi getWriteApi() {
		if (writeApi == null) {
			writeApi = influxDB.makeWriteApi();
		}
		return writeApi;
	}

	/**
	 * Creates the configured database in influx if it does not exist yet.
	 */
	private Bucket createDatabaseIfNotExistent() {
		Bucket bucket = getBucketsApi().findBucketByName(influxDBConfig.getInfluxDatabase());
		if (Objects.isNull(bucket)) {
			bucket = getBucketsApi().createBucket(influxDBConfig.getInfluxDatabase(), influxDBConfig.getInfluxOrgId());
		}
		return bucket;
	}

	/**
	 * Parses list of samplers.
	 *
	 * @param context
	 *            {@link BackendListenerContext}.
	 */
	private void parseSamplers(BackendListenerContext context) {
		samplersList = context.getParameter(KEY_SAMPLERS_LIST, "");
		samplersToFilter = new HashSet<String>();
		if (context.getBooleanParameter(KEY_USE_REGEX_FOR_SAMPLER_LIST, false)) {
			regexForSamplerList = samplersList;
		} else {
			regexForSamplerList = null;
			String[] samplers = samplersList.split(SEPARATOR);
			samplersToFilter = new HashSet<String>();
			for (String samplerName : samplers) {
				samplersToFilter.add(samplerName);
			}
		}
	}


	@Override
	/**
	 * Processes sampler results.
	 */
	public void handleSampleResults(List<SampleResult> sampleResults, BackendListenerContext context) {
		// Gather only regex results to array
		sampleResults.forEach(it->{
			//write every filtered result to array
			if (checkFilter(it)) {
				getUserMetrics().add(it);
				allSampleResults.add(it);
			}
			if (recordSubSamples) {
				//write every filtered sub_result to array
				for (SampleResult subResult : it.getSubResults()) {
					if (checkFilter(subResult)) {
						getUserMetrics().add(subResult);
						allSampleResults.add(subResult);
					}
				}
			}
		});

		//Flush point(s) every group by Count
		if (groupBy) {
			if (allSampleResults.size() >= groupByCount) {
				flushPoints();
			}
		} else {
			//or every point
			flushPoint();
		}
	}

	private boolean checkFilter(SampleResult sample){
		return 	((null != regexForSamplerList && sample.getSampleLabel().matches(regexForSamplerList)) || samplersToFilter.contains(sample.getSampleLabel()));
	}

	//Save one-item-array to DB
	private void flushPoint()
	{
		allSampleResults.forEach(it -> {
			Point point = Point.measurement(RequestMeasurement.MEASUREMENT_NAME).time(
							System.currentTimeMillis() * ONE_MS_IN_NANOSECONDS + getUniqueNumberForTheSamplerThread(), WritePrecision.MS)
					.addTag(RequestMeasurement.Tags.REQUEST_NAME, it.getSampleLabel())
					.addField(RequestMeasurement.Fields.ERROR_COUNT, it.getErrorCount())
					.addField(RequestMeasurement.Fields.THREAD_NAME, it.getThreadName())
					.addTag(RequestMeasurement.Tags.RUN_ID, runId)
					.addTag(RequestMeasurement.Tags.TEST_NAME, testName)
					.addField(RequestMeasurement.Fields.NODE_NAME, nodeName)
					.addField(RequestMeasurement.Fields.POINTS_COUNT, 1)
					.addField(RequestMeasurement.Fields.RESPONSE_TIME, it.getTime());

			getWriteApi().writePoint(influxDBConfig.getInfluxDatabase(), influxDBConfig.getInfluxOrgId(), point);
		});
		allSampleResults.clear();
	}

	//Aggregate and Save array to DB
	private void flushPoints()
	{
		//group aggregation
		final Map<String,JMPoint> samplesTst = allSampleResults.stream().collect(Collectors.groupingBy(SampleResult::getSampleLabel,
				Collectors.collectingAndThen(Collectors.toList(), list -> {
							long errorsCount = list.stream().collect(Collectors.summingLong(SampleResult::getErrorCount));
							long count = list.stream().collect(Collectors.counting());
							double average = list.stream().collect(Collectors.averagingDouble(SampleResult::getTime));
							return new JMPoint("aggregate",errorsCount,count, average);
						}
				)));
		//save aggregates to DB
		samplesTst.forEach((pointName,pointData)-> {
			Point point = Point.measurement(RequestMeasurement.MEASUREMENT_NAME).time(
							System.currentTimeMillis() * ONE_MS_IN_NANOSECONDS + getUniqueNumberForTheSamplerThread(), WritePrecision.MS)
					.addTag(RequestMeasurement.Tags.REQUEST_NAME, pointName)
					.addField(RequestMeasurement.Fields.ERROR_COUNT, pointData.getErrorCount())
					.addField(RequestMeasurement.Fields.THREAD_NAME, pointData.getThreadName())
					.addTag(RequestMeasurement.Tags.RUN_ID, runId)
					.addTag(RequestMeasurement.Tags.TEST_NAME, testName)
					.addField(RequestMeasurement.Fields.NODE_NAME, nodeName)
					.addField(RequestMeasurement.Fields.POINTS_COUNT, pointData.getPointsCount())
					.addField(RequestMeasurement.Fields.RESPONSE_TIME, pointData.getAverageTimeInt());
			getWriteApi().writePoint(influxDBConfig.getInfluxDatabase(), influxDBConfig.getInfluxOrgId(), point);
		});
		//so now we can clean array
		allSampleResults.clear();
	}

	/**
	 * Try to get a unique number for the sampler thread
	 */
	private int getUniqueNumberForTheSamplerThread() {
		return randomNumberGenerator.nextInt(ONE_MS_IN_NANOSECONDS);
	}

}
