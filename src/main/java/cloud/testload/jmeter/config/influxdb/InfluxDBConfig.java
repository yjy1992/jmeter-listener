package cloud.testload.jmeter.config.influxdb;

import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.visualizers.backend.BackendListenerContext;

/**
 * Configuration for influxDB.
 *
 * @author Alexander Wert
 *
 */
public class InfluxDBConfig {

	/**
	 * Default database name.
	 */
	public static final String DEFAULT_DATABASE = "jmresults";

	/**
	 * Default retention policy name.
	 */
	public static final String DEFAULT_RETENTION_POLICY = "autogen";

	/**
	 * Default port.
	 */
	public static final int DEFAULT_PORT = 8086;

	/**
	 * Config key for database name.
	 */
	public static final String KEY_INFLUX_DB_DATABASE = "influxDBDatabase";

	/**
	 * Config key for password.
	 */
	public static final String KEY_INFLUX_DB_PASSWORD = "influxDBPassword";

	/**
	 * Config key for user name.
	 */
	public static final String KEY_INFLUX_DB_USER = "influxDBUser";

	/**
	 * Config key for port.
	 */
	public static final String KEY_INFLUX_DB_PORT = "influxDBPort";

	/**
	 * Config key for host.
	 */
	public static final String KEY_INFLUX_DB_HOST = "influxDBHost";

	/**
	 * Config key for token.
	 */
	public static final String KEY_INFLUX_DB_TOKEN = "influxDBToken";

	/**
	 * Config key for orgId.
	 */
	public static final String KEY_INFLUX_DB_ORG_ID = "influxDBOrgId";

	/**
	 * Config key for orgName.
	 */
	public static final String KEY_INFLUX_DB_ORG_NAME = "influxDBOrgName";

	/**
	 * Config key for retention policy name.
	 */
	public static final String KEY_RETENTION_POLICY = "retentionPolicy";

	/**
	 * InfluxDB Host.
	 */
	private String influxDBHost;

	/**
	 * InfluxDB User.
	 */
	private String influxUser;

	/**
	 * InfluxDB Password.
	 */
	private String influxPassword;

	/**
	 * InfluxDB Token.
	 */
	private String influxToken;

	/**
	 * InfluxDB OrgId.
	 */
	private String influxOrgId;

	/**
	 * InfluxDB OrgName.
	 */
	private String influxOrgName;

	/**
	 * InfluxDB database name.
	 */
	private String influxDatabase;

	/**
	 * InfluxDB database retention policy.
	 */
	private String influxRetentionPolicy;

	/**
	 * InfluxDB Port.
	 */
	private int influxDBPort;

	public static final String VERSION_V1 = "V1";

	public static final String VERSION_V2 = "V2";

	public InfluxDBConfig(BackendListenerContext context, String version) {
		String influxDBHost = context.getParameter(KEY_INFLUX_DB_HOST);
		if (StringUtils.isEmpty(influxDBHost)) {
			throw new IllegalArgumentException(KEY_INFLUX_DB_HOST + "must not be empty!");
		}
		setInfluxDBHost(influxDBHost);

		int influxDBPort = context.getIntParameter(KEY_INFLUX_DB_PORT, InfluxDBConfig.DEFAULT_PORT);
		setInfluxDBPort(influxDBPort);

		String influxUser = context.getParameter(KEY_INFLUX_DB_USER);
		setInfluxUser(influxUser);

		String influxPassword = context.getParameter(KEY_INFLUX_DB_PASSWORD);
		setInfluxPassword(influxPassword);

		String influxToken = context.getParameter(KEY_INFLUX_DB_TOKEN);
		if (VERSION_V2.equals(version) && StringUtils.isEmpty(influxToken)) {
			throw new IllegalArgumentException(KEY_INFLUX_DB_TOKEN + " must not be empty when version is V2.");
		}
		setInfluxToken(influxToken);

		String influxOrgId = context.getParameter(KEY_INFLUX_DB_ORG_ID);
		if (VERSION_V2.equals(version) && StringUtils.isEmpty(influxToken)) {
			throw new IllegalArgumentException(KEY_INFLUX_DB_ORG_ID + " must not be empty when version is V2.");
		}
		setInfluxOrgId(influxOrgId);

		String influxOrgName = context.getParameter(KEY_INFLUX_DB_ORG_NAME);
		setInfluxOrgName(influxOrgName);

		String influxDatabase = context.getParameter(KEY_INFLUX_DB_DATABASE);
		if (StringUtils.isEmpty(influxDatabase)) {
			throw new IllegalArgumentException(KEY_INFLUX_DB_DATABASE + "must not be empty!");
		}
		setInfluxDatabase(influxDatabase);

		String influxRetentionPolicy = context.getParameter(KEY_RETENTION_POLICY, DEFAULT_RETENTION_POLICY);
		if (StringUtils.isEmpty(influxRetentionPolicy)) {
			influxRetentionPolicy = DEFAULT_RETENTION_POLICY;
		}
		setInfluxRetentionPolicy(influxRetentionPolicy);
	}

	/**
	 * Builds URL to influxDB.
	 *
	 * @return influxDB URL.
	 */
	public String getInfluxDBURL() {
		return "http://" + influxDBHost + ":" + influxDBPort;
	}

	/**
	 * Builds URL to influxDB.
	 *
	 * @return influxDB URL.
	 */
	public String getInfluxDBURL(String version) {
		if (VERSION_V1.equals(version)) {
			return "http://" + influxDBHost + ":" + influxDBPort;
		} else if (VERSION_V2.equals(version)) {
			return "http://" + influxDBHost + ":" + influxDBPort + "?readTimeout=5000";
		} else {
			throw new RuntimeException("Wrong version");
		}
	}

	/**
	 * @return the influxDBHost
	 */
	public String getInfluxDBHost() {
		return influxDBHost;
	}

	/**
	 * @param influxDBHost
	 *            the influxDBHost to set
	 */
	public void setInfluxDBHost(String influxDBHost) {
		this.influxDBHost = influxDBHost;
	}

	/**
	 * @return the influxUser
	 */
	public String getInfluxUser() {
		return influxUser;
	}

	/**
	 * @param influxUser
	 *            the influxUser to set
	 */
	public void setInfluxUser(String influxUser) {
		this.influxUser = influxUser;
	}

	/**
	 * @return the influxPassword
	 */
	public String getInfluxPassword() {
		return influxPassword;
	}

	/**
	 * @param influxPassword
	 *            the influxPassword to set
	 */
	public void setInfluxPassword(String influxPassword) {
		this.influxPassword = influxPassword;
	}

	public String getInfluxToken() {
		return influxToken;
	}

	public void setInfluxToken(String influxToken) {
		this.influxToken = influxToken;
	}

	public String getInfluxOrgId() {
		return influxOrgId;
	}

	public void setInfluxOrgId(String influxOrgId) {
		this.influxOrgId = influxOrgId;
	}

	public String getInfluxOrgName() {
		return influxOrgName;
	}

	public void setInfluxOrgName(String influxOrgName) {
		this.influxOrgName = influxOrgName;
	}

	/**
	 * @return the influxDatabase
	 */
	public String getInfluxDatabase() {
		return influxDatabase;
	}

	/**
	 * @param influxDatabase
	 *            the influxDatabase to set
	 */
	public void setInfluxDatabase(String influxDatabase) {
		this.influxDatabase = influxDatabase;
	}

	/**
	 * @return the influxRetentionPolicy
	 */
	public String getInfluxRetentionPolicy() {
		return influxRetentionPolicy;
	}

	/**
	 * @param influxRetentionPolicy
	 *            the influxRetentionPolicy to set
	 */
	public void setInfluxRetentionPolicy(String influxRetentionPolicy) {
		this.influxRetentionPolicy = influxRetentionPolicy;
	}

	/**
	 * @return the influxDBPort
	 */
	public int getInfluxDBPort() {
		return influxDBPort;
	}

	/**
	 * @param influxDBPort
	 *            the influxDBPort to set
	 */
	public void setInfluxDBPort(int influxDBPort) {
		this.influxDBPort = influxDBPort;
	}
}
