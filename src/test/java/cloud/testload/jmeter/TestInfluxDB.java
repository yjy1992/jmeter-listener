package cloud.testload.jmeter;

import org.apache.jmeter.visualizers.backend.BackendListenerContext;

/**
 * @Description:
 * @author: 夜七天
 * @create: 2024/05/09 15:07
 */
public class TestInfluxDB {

    public static void main(String[] args) throws Exception {
        InfluxDBV2BackendListenerClient client = new InfluxDBV2BackendListenerClient();
        BackendListenerContext context = new BackendListenerContext(client.getDefaultParameters());
        client.setupTest(context);
        System.out.println(client);
    }
}
